Rails.application.routes.draw do
  devise_for :users, :controllers => {registrations: 'registrations'}
  resources :events, :only => [:index, :new, :create, :show, :edit, :update]

  get '/moremoney', to: 'tickets#more_money'
  post '/ticket/markForDelete/:id', to: 'tickets#set_to_delete'

  get '/tickets/show_filtered/:from/:to', to: 'tickets#filter'

  resources :tickets
  root :to => "events#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

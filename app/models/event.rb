class Event < ApplicationRecord
	validates :artist, presence: true
	validates :price_low, presence: true
	validates :price_high, presence: true
	validates :event_date, presence: true
	validates :event_date, presence: true
	validates :sell_start_date, presence: true
	validate :event_date_not_from_past
	validates :price_high, numericality: true
    validates :price_low, numericality: true


    has_many :tickets

	def event_date_not_from_past
		if !event_date.nil?
		    if event_date< Date.today
		      errors.add('Event date ','cannot be from the past')
		    end
		end
 	end

end

class Ticket < ApplicationRecord
	validates :name, presence: true 
	validates :name, length: { :minimum => 6 }
	validates :email_address, presence: true
	validates_format_of :email_address ,:with => Devise::email_regexp
	validates :price, presence: true
	validates :price, numericality: true
	validates :amount, numericality: {only_integer: true, greater_than_or_equal_to: 1}
	validates :phone, presence: true
	validates :phone, length: {:minimum => 8}
	validate :not_too_young
	validate :not_too_many_tickets
	validate :has_money
	validate :can_be_bought
	validate :not_decreasing_amount


	belongs_to :event
	belongs_to :user

	def not_too_young
		userDate = User.where(id: user_id)[0]
		event = Event.where(id: event_id)[0]
		if userDate.nil?
			errors.add('User ',' did not put date of birth')
		end
		if(userDate.date_of_birth > event.min_age.year.ago)
			errors.add('User ','is to young for this show')
		end
	end

	def not_decreasing_amount
		ticket = Ticket.where(id: id)[0]
		if !ticket.nil?
			if ticket.amount > amount
				errors.add('Amount: ',' cannot decrease amount')
			end
		end
	end

	def are_spots_empty
		event = Event.where(id: event_id)[0]
		if event.nil?
			errors.add('Amount: ',' not enaugh tickets left')
		end
		if event.spots - event.spots_sold < amount
			errors.add('Amount: ',' not enough tickets left')
		end
	end

	def not_too_many_tickets
    	sum = amount
    	tickets = Ticket.where(user_id: user_id, event_id: event_id)
    	tickets.each {|x| 
    		if x.id != id
    			sum+=x.amount
    		end
    	}
    	if sum > 5
    		errors.add('Amount',': To many tickets bought. Max 5 for event')
    	end
    end

    def has_money
    	moneyNeeded = price*amount;
    	ticket = Ticket.find_by(id: id)
    	if !ticket.nil?
    		moneyNeeded -= ticket.price * ticket.amount
    	end
    	moneyAvailable = User.find(user_id).money
    	if moneyNeeded>moneyAvailable
    		errors.add('Price',': You do not have enough money.')
    	end
    end

    def can_be_bought
    	event = Event.find(event_id)
    	if(event.sell_start_date>Date.today || event.event_date < Date.today)
    		errors.add('Date', 'You cannot buy tickets for this event now')
    	end
    end
end

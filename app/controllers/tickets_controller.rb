class TicketsController < ApplicationController
  before_action :set_ticket, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]
  before_action :correct_user, only: [:edit, :update, :set_to_delete]
  before_action :only_admin, only: [:destroy]
  # GET /tickets
  # GET /tickets.json
  def index
    if(current_user.role == 'admin')
      @tickets = Ticket.all
    else
      @tickets = current_user.tickets
    end
  end

  # GET /tickets/1
  # GET /tickets/1.json
  def show
  end

  # GET /tickets/new
  def new
    @ticket = Ticket.new(user_id: current_user.id)
  end

  # GET /tickets/1/edit
  def edit
  end

  # POST /tickets
  # POST /tickets.json
  def create
    @ticket = Ticket.new(ticket_params)
    event = Event.find(@ticket.event_id)
    event.spots_sold += @ticket.amount
    user = User.find(@ticket.user_id)
    user.money -= @ticket.price*@ticket.amount
    respond_to do |format|
      if @ticket.save
        user.save
        event.save
        format.html { redirect_to @ticket, notice: 'Ticket was successfully created.' }
        format.json { render :show, status: :created, location: @ticket }
      else
        format.html { render :new }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tickets/1
  # PATCH/PUT /tickets/1.json
  def update
    respond_to do |format|
    event = Event.find(@ticket.event_id)
    prevAmount = @ticket.amount
    user = User.find(@ticket.user_id)
      if @ticket.update(ticket_params)
        event.spots_sold -=prevAmount - @ticket.amount
        event.save
        user.money += (prevAmount - @ticket.amount) * @ticket.price
        user.save
        format.html { redirect_to @ticket, notice: 'Ticket was successfully updated.' }
        format.json { render :show, status: :ok, location: @ticket }
      else
        format.html { render :edit }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    event = Event.find(@ticket.event_id)
    event.spots_sold -= @ticket.amount
    user = User.find(@ticket.user_id)
    user.money += @ticket.price*@ticket.amount*return_percent_money(event.event_date)
    @ticket.destroy
    respond_to do |format|
      event.save
      user.save
      format.html { redirect_to tickets_url, notice: 'Ticket was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def set_to_delete
    ticket = Ticket.find(params[:id])
    ticket.to_delete = true
    ticket.save
    @tickets = current_user.tickets
    render :index, notice: 'Ticket was marked for cancelation'
  end

  def filter
    dateFrom = Date.parse(params[:form])
  end

  def more_money
    user = current_user
    current_user.money += 100
    current_user.save
    redirect_to '/' , notice: 'Money increased'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.find(params[:id])
    end

    def goodAmount
      sum = ticket_params[:amount].to_i
      if sum > 5
        flash[:notice] = "You cannot buy more than 5 tickets for one event"
        render 
      end
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def ticket_params
      params.require(:ticket).permit(:price, :email_address, :phone,:event_id, :user_id, :amount, :name,:from)
    end
    def correct_user
      if(current_user.role != 'admin')
        @ticket = current_user.tickets.find_by(id: params[:id])
        redirect_to tickets_path, notice: "Nie jesteś uprawniony do edycji tego biletu" if @ticket.nil?
      end
    end
    def only_admin
      if(current_user.role != 'admin')
        redirect_to tickets_path, notice: "Nie jesteś uprawniony do edycji tego biletu"
      end
    end

    def return_percent_money (date)
      if(30.days.from_now < date)
        return 0.6
      elsif 15.days.from_now < date
        return 0.5
      elsif 7.days.from_now < date
        return 0.3
      elsif 3.days.from_now < date
        return 0.2
      else
        return 0.1
      end
    end
end

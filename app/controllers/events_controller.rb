class EventsController < ApplicationController

	before_action :set_event, only: [:show, :edit, :update]
  before_action :authenticate_user!, except: [:index, :show]
  before_action :is_admin, only: [:edit, :update]
	#before_action :check_logged_in, :only => [:new, :create]

  def new
  	@event = Event.new
  end

  def create
  	@event = Event.new(event_params)
      if @event.save
        flash[:komunikat] = 'Ticket was successfully created.'
        redirect_to "/events/#{@event.id}", notice: 'Event was successfully created.'
      else
       	render :new
      end
  end

  def index
    @events = Event.all
  end

  def update
      if @event.update(event_params)
        redirect_to @event, notice: 'Event was successfully updated.' 
      else
        format.html { render :edit }
      end
  end

  def edit
  end

  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    def event_params
      params.require(:event).permit(:artist, :description,:price_low, :price_high, :event_date, :spots, :spots_sold, :min_age, :sell_start_date)
    end

    def is_admin
      redirect_to events_path, notice: "Nie jesteś uprawniony do edycji tego biletu" if current_user.nil? || current_user.role != 'admin'
    end
end

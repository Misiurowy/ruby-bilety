class RegistrationsController < Devise::RegistrationsController

  private

  def sign_up_params
    params.require(:user).permit(:phone, :date_of_birth, :role, :money, :email, :password, :password_confirmation)
  end

  def account_update_params
    params.require(:user).permit(:phone, :date_of_birth, :role, :money, :email, :password, :password_confirmation, :current_password)
  end
end
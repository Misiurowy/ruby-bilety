class RemoveAmountFromTickets < ActiveRecord::Migration[5.1]
  def change
    remove_column :tickets, :amount, :string
  end
end

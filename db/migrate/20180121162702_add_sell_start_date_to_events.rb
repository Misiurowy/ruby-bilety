class AddSellStartDateToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :sell_start_date, :date
    add_column :events, :spots, :integer
    add_column :events, :spots_sold, :integer
    add_column :events, :min_age, :integer
    add_column :users, :date_of_birth, :date 
  end
end

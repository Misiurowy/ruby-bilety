class RemoveNameFromTickets < ActiveRecord::Migration[5.1]
  def change
    remove_column :tickets, :name, :string
    remove_column :tickets, :seat_id_seq, :string
    remove_column :tickets, :address, :text
  end
end
